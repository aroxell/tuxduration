#!/bin/bash

root_dir=$(readlink -f $(dirname $0))

tmpfile=$(mktemp /tmp/abc-script.XXXXXX)
git_url=https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git
PLAN=${PLAN:-tuxplan.yml}
output_dir=${root_dir}/
no_cache=''

function get_tag(){
	local release=${1}
	local rc=${2}
	local done=${3}
	for i in $(seq 1 $rc); do
		echo ${release}-rc$i >> ${tmpfile}
	done
	if [[ ${done} == 'true' ]]; then
		echo ${release} >> ${tmpfile}
	fi
}

function usage() {
	echo "Usage: $(basename "$0") [-c ] -i value [-o outputdir]"
	echo "-c : If set tuxsuite builds without cache."
	echo "-i <value> : <value> can be either 'build' or 'results'."
	echo "-o <outputdir> : <outputdir> where your output files will be dumped."
}

while getopts "hci:o:" opt; do
	case $opt in
		c)
			no_cache='--no-cache'
			;;
		i)
			input=${OPTARG}
			;;
		o)
			output_dir=${OPTARG}
			;;
		h)
			usage
			exit 0
			;;
	esac
done

if [[ ${input} != "build" && ${input} != "results" ]]; then
	echo "input ${input} has to be 'build' or 'results'"
	exit 1
fi
mkdir -p ${output_dir}

get_tag v5.14 7 true
get_tag v5.15 7 true
get_tag v5.16 8 true
get_tag v5.17 1 false

counter=0
for tag in $(cat ${tmpfile}); do
	file=$(echo ${git_url}|awk -F'/' '{print $NF}'|awk -F'.' '{print $1}')-${tag}
	if [[ ${input} == "build" ]]; then
		mkdir -p ${output_dir}/in
		tuxsuite plan --git-repo ${git_url} --git-ref ${tag} ${PLAN} --json-out ${output_dir}/in/${file}.json --no-wait ${no_cache}
	elif [[ ${input} == "results" ]]; then
		mkdir -p ${output_dir}/out
		tuxsuite results --from-json ${output_dir}/in/${file}.json --json-out ${output_dir}/out/${counter}-${file}.json
	fi
	let counter+=1
done

rm ${tmpfile}
