# tuxduration

Host Linux kernel build-time graphs (real-time) to aide the Linux kernel community.

## Usage

```
Usage: ./build.sh -i (build|results) [-o output-dir]
./build.sh -i build	 : builds and tests kernels in tuxsuite, output dir
			   'in' with files.
	   -i results	 : pull's the result from tuxsuite using the files in
			   'in' and creates files in the 'out' dir.
	   -o output-dir : Default output-dir is the root of the scripts.

Usage: ./generate-txt.py [--output-dir DIR]
./generate-txt.py	 : pull's out the needed data from the 'out' dir and
			   generates txt files in the 'txt' dir.
	--output-dir DIR : Default output-dir is the root of the scripts.

Usage: ./process.py [--output-dir DIR]
./process.py		 : process the txt files in 'txt' dir and generate
			   json files in the 'stats' dir with stats like:
			   'mean', 'fmean','geometric_mean','harmonic_mean',
			   'median','median_low','median_high','median_grouped',
			   'mode','multimode','quantiles','pstdev','pvariance',
			   'stdev','variance'.
	--output-dir DIR : Default output-dir is the root of the scripts.

Usage: ./graph-data.py [--output-dir DIR]
./graph-data.py		 : generates data to draw graph's from in the dir
			   'graph-data'.
	--output-dir DIR : Default output-dir is the root of the scripts.

Usage: ./graph.py [--output-dir DIR]
./graph.py		 : generates png files with graph's in the 'images'
			   dir.
	--output-dir DIR : Default output-dir is the root of the scripts.
	--stat STAT	 : Statistics to plot except Standard Diviation (stdev).
			   Choose one of the following: 'mean', 'harmonic_mean',
			   'median', 'pstdev', 'pvariance', 'variance'.
```

## Requirements
Requirements needed to be installed, see file requirements.txt.
```
pip3 install -U --user matplotlib
```

## Example

How to run the example data:
```
./generate-txt.py --output-dir example/data
./process.py --output-dir example/data
./graph-data.py --output-dir example/data
./graph.py --output-dir example/data
```
