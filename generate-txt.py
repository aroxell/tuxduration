#!/usr/bin/python3

import json
import os
import argparse

def generate_line(data, dfilter):
    line = ""
    for key,value in data[dfilter].items():
        if dfilter == 'builds':
            if not value['build_status'].endswith("error"):
                line += (f"{value['git_describe']}-"
                        f"{value['target_arch']}-"
                        f"{value['kconfig'][0]}-"
                        f"{value['toolchain']},"
                        f"{value['target_arch']},"
                        f"{value['toolchain']},"
                        f"{value['build_status']},"
                        f"{value['git_describe']},"
                        f"{value['kconfig'][0]},"
                        f"{value['uid']},"
                        f"device,"
                        f"{os.path.basename(file).split('-')[0]},"
                        f"{value['duration']}\n"
                        )
        elif dfilter == 'tests':
            if not value['result'].endswith("fail"):
                line += (f"{value['build']['git_describe']}-"
                        f"{value['device']}-"
                        f"{value['build']['kconfig'][0]}-"
                        f"{value['build']['toolchain']},"
                        f"{value['build']['target_arch']},"
                        f"{value['build']['toolchain']},"
                        f"{value['result']},"
                        f"{value['build']['git_describe']},"
                        f"{value['build']['kconfig'][0]},"
                        f"{value['uid']},"
                        f"{value['device']},"
                        f"{os.path.basename(file).split('-')[0]},"
                        f"{value['duration']}\n"
                        )
    return line


parser = argparse.ArgumentParser(description="Generate txt files in the 'txt' dir from the 'out' directory")

parser.add_argument(
    "--output-dir",
    help="Where the output directories and files will be dumped.",
)

args = parser.parse_args()
output_dir = args.output_dir or os.path.dirname(__file__)

for root, dirs, files in os.walk(os.path.join(output_dir, "out")):
    for file in files:
        if file.endswith(".json"):
            inputfile = os.path.join(root, file)

            with open(inputfile) as json_file:
                data = json.load(json_file)
                for key,value in data['tests'].items():
                    data['tests'][key]['build'] = data['builds'][value['waiting_for']]

                line = generate_line(data, 'builds')

                root_output_dir = os.path.join(output_dir, 'txt')
                if not os.path.exists(root_output_dir):
                    os.makedirs(root_output_dir)

                outfile = open(os.path.join(root_output_dir, 'builds-' + os.path.splitext(file)[0] + '.txt'), "w")
                outfile.write(line)
                outfile.close()

                line = generate_line(data, 'tests')

                outfile = open(os.path.join(root_output_dir, 'tests-' + os.path.splitext(file)[0] + '.txt'), "w")
                outfile.write(line)
                outfile.close()
