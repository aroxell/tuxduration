#!/usr/bin/python3

import json
import os
from collections import defaultdict
import statistics
import argparse

parser = argparse.ArgumentParser(description="Generate stats files in the 'stats' dir from the 'txt' directory")

parser.add_argument(
    "--output-dir",
    help="Where the output directories and files will be dumped.",
)

args = parser.parse_args()
output_dir = args.output_dir or os.path.dirname(__file__)

for root, dirs, files in os.walk(os.path.join(output_dir, "txt")):
    for file in files:
        if file.endswith(".txt"):
            inputfile = os.path.join(root, file)

            d = defaultdict(dict)
            with open(inputfile, 'r') as txt_file:
                contents = txt_file.readlines()

                mystats = defaultdict(list)
                for line in contents:
                    line = line.strip('\n')
                    name, target_arch, toolchain, result, git_describe, kconfig, uid, device, count, duration = line.split(",")
                    if 'builds' not in d[name]:
                        d[name]['builds'] = list()
                    d[name]['builds'].append({
                        'uid': uid,
                        'duration': duration,
                    })
                    d[name].update({
                        'target_arch': target_arch,
                        'toolchain': toolchain,
                        'git_describe': git_describe,
                        'kconfig': kconfig,
                        'result': result,
                        'count': int(count),
                    })
                    if 'device' not in d[name]:
                        d[name].update({
                            'device': device,
                        })
                    mystats[name].append(int(duration))

                for name,durations in mystats.items():
                    for method in ['mean', 'fmean','geometric_mean','harmonic_mean','median','median_low','median_high','median_grouped','mode','multimode','quantiles','pstdev','pvariance','stdev','variance']:
                        func = getattr(statistics,method)
                        d[name].update({
                                method: func(durations)
                        })

                json_object = json.dumps(d, indent = 4) 
                root_output_dir = os.path.join(output_dir, 'stats')
                if not os.path.exists(root_output_dir):
                    os.makedirs(root_output_dir)

                outfile = open(os.path.join(root_output_dir, os.path.splitext(file)[0] + '.json'), "w")
                outfile.write(json_object)
                outfile.close()
