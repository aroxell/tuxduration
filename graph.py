#!/usr/bin/python3

import json
import os
from collections import defaultdict
import matplotlib.pyplot as plt
import argparse

parser = argparse.ArgumentParser(description="Generate txt files in the 'txt' dir from the 'out' directory")

parser.add_argument(
    "--output-dir",
    help="Where the output directories and files will be dumped.",
)

parser.add_argument(
    "--stat",
    help="Statistics to plot except Standard Diviation (stdev). Choose one of the following: 'mean', 'harmonic_mean', 'median', 'pstdev', 'pvariance', 'stdev', 'variance'",
)

args = parser.parse_args()
output_dir = args.output_dir or os.path.dirname(__file__)
stat = args.stat or 'mean'

root_output_dir = os.path.join(output_dir, 'images')
if not os.path.exists(root_output_dir):
    os.makedirs(root_output_dir)

for root, dirs, files in os.walk(os.path.join(output_dir, "graph-data")):
    for file in files:
        if file.endswith(".json"):
            inputfile = os.path.join(root, file)

            with open(inputfile, 'r') as json_file:
                data = json.load(json_file)
                for key, value in data.items():
                    builds = sorted(value, key=lambda value: value['count'])
                    xAxis = [build["git_describe"] for build in builds]
                    yAxis = [build[stat] for build in builds]
                    zAxis = [build["stdev"] for build in builds]
                    plt.grid(True)

                    plt.plot(xAxis, yAxis, color='maroon', marker='o', label=stat)
                    plt.plot(xAxis, zAxis, color='red', marker='o', label='std')
                    plt.xlabel('Kernel releases')
                    plt.ylabel('Seconds')

                    plt.xticks(rotation=45, ha='right')

                    plt.title(label=stat + " " + os.path.splitext(file)[0],
                                fontweight=10,
                                color="black")

                    plt.legend()

                    plt.savefig(os.path.join(root_output_dir, stat + "-" + os.path.splitext(file)[0]))
                    plt.clf()
