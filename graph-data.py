#!/usr/bin/python3

import json
import os
from collections import defaultdict
import matplotlib.pyplot as plt
import argparse

parser = argparse.ArgumentParser(description="Generate stats files in the 'stats' dir from the 'txt' directory")

parser.add_argument(
    "--output-dir",
    help="Where the output directories and files will be dumped.",
)

args = parser.parse_args()
output_dir = args.output_dir or os.path.dirname(__file__)

d = defaultdict(dict)
for root, dirs, files in os.walk(os.path.join(output_dir, "stats")):
    for file in files:
        if file.endswith(".json"):
            inputfile = os.path.join(root, file)

            with open(inputfile, 'r') as json_file:
                data = json.load(json_file)
                for key,value in data.items():

                    if not value['device'].endswith('device'):
                        name = (f"{value['device']}-")
                    else:
                        name = (f"{value['target_arch']}-")
                    name += (
                        f"{value['kconfig']}-"
                        f"{value['toolchain']}"
                    )
                    if 'builds' not in d[name]:
                        d[name]['builds'] = list()
                    d[name]['builds'].append({
                        'count': value['count'],
                        'git_describe': value['git_describe'],
                        'mean': value['mean'],
                        'harmonic_mean': value['harmonic_mean'],
                        'median': value['median'],
                        'pstdev': value['pstdev'],
                        'pvariance': value['pvariance'],
                        'stdev': value['stdev'],
                        'variance': value['variance'],
                        'device': value['device'],
                    })

json_object = json.dumps(d, indent = 4)
outfile = open(os.path.join(output_dir, 'graph.json'), "w")
outfile.write(json_object)
outfile.close()

root_output_dir = os.path.join(output_dir, 'graph-data')
if not os.path.exists(root_output_dir):
    os.makedirs(root_output_dir)

xAxis = dict()
yAxis = dict()
for key, value in d.items():
    tmp_json_object = json.dumps(value, indent = 4)
    outfile = open(os.path.join(root_output_dir, key + '.json'), "w")
    outfile.write(tmp_json_object)
    outfile.close()
